
import React, {Component} from 'react';  
import CityTable from './CityTable';
import { simplify } from 'simplifr';

class CityForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: '', data: ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {    this.setState({value: event.target.value});  }

  handleSubmit(event) {
    event.preventDefault();

    // fetch('http://172.19.0.3:8080/test', { headers: {'Content-Type':'application/json'}} )
    fetch('http://localhost:8080/info?city=' + this.state.value, { headers: {'Content-Type':'application/json'}} )
    .then(res => res.json())
    .then((data) => {
      console.log(data);
      if (data.status == 404) {
        alert('City not found!');
        return '';
      }
      return data;
    })
    .then(responseData => {
      this.setState({
        data : responseData
      })
    });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" value={this.state.value} onChange={this.handleChange} /><br/>
          <input id="SubmitButton" type="submit" value="Submit" />
        </form>
        {this.state.data != '' && <CityTable data={this.state.data.data} />}
      </div>
    );
  }

}

export default CityForm;
  