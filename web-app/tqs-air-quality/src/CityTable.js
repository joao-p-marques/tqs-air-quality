
import React from 'react';

export default class CityTable extends React.Component {
 
    constructor(props){
        super(props);
        this.getHeader = this.getHeader.bind(this);
        this.getRowsData = this.getRowsData.bind(this);
        this.getKeys = this.getKeys.bind(this);
    }
    
    getKeys = function(){
        return Object.keys(this.props.data);
    }
    
    getValues = function(){
        return Object.values(this.props.data);
    }
    
    getHeader = function(){
        return [<th key="key">Key</th>, <th key="Value">Value</th>];
    }
    
    getRowsData = function(){
        var keys = this.getKeys();
        const rows = [];
        Object.keys(this.props.data).forEach(key => rows.push(
            <tr key={key}><RenderRow key={key} data={this.props.data[key]} keys={keys}/></tr>
        ))
        console.log(rows);
        // return rows;
        return rows.map(x => {
            return x;
        })
    }

    getData = function() {
        var data = this.props.data;

        var rows = [
            <tr key="CityName"><td>City Name</td><td key="CityName">{data.city.name}</td></tr>,
            <tr key="API_ID"><td>External API ID</td><td key="API_ID">{data.aqi}</td></tr>,
            <tr key="Lat"><td>Latitude</td><td key="Lat">{data.city.geo[0]}</td></tr>,
            <tr key="Long"><td>Longitude</td><td key="Long">{data.city.geo[1]}</td></tr>,
            <tr key="DominantPol"><td>Dominant Pollutant</td><td key="DominantPol">{data.dominentpol}</td></tr>,
            <tr key="CO"><td>Carbon Monoxide</td><td key="CO">{data.iaqi.co.v}</td></tr>,
            <tr key="H"><td>Humidity</td><td key="H">{data.iaqi.h.v}</td></tr>,
            <tr key="NO2"><td>Nitrogen Dioxide</td><td key="NO2">{data.iaqi.no2.v}</td></tr>,
            <tr key="O3"><td>Ozone</td><td key="O3">{data.iaqi.o3.v}</td></tr>,
            <tr key="PM10"><td>PM10</td><td key="PM10">{data.iaqi.pm10.v}</td></tr>,
            <tr key="PM25"><td>PM25</td><td key="PM25">{data.iaqi.pm25.v}</td></tr>,
            <tr key="SO2"><td>Sulfur Dioxide</td><td key="SO2">{data.iaqi.so2.v}</td></tr>,
            <tr key="T"><td>Temperature</td><td key="T">{data.iaqi.t.v}</td></tr>,
            <tr key="H"><td>Humidity</td><td key="H">{data.iaqi.h.v}</td></tr>,
            <tr key="P"><td>Pressure</td><td key="P">{data.iaqi.p.v}</td></tr>,
            <tr key="W"><td>Wind</td><td key="W">{data.iaqi.w.v}</td></tr>,
            <tr key="Time"><td>Time</td><td key="Time">{data.time.s}</td></tr>
        ]

        return rows;
    }
    
    render() {
        console.log(this.props.data);

        // build table one by one

        return (
            <div>
                <table>
                    <thead>
                        <tr>{this.getHeader()}</tr>
                    </thead>
                    <tbody>
                        {this.getData()}
                    </tbody>
                </table>
            </div>
        );
    }
}

const RenderRow = (props) =>{
    return props.keys.map((key, index)=>{
        return <td key={props.data[key]}>{props.data[key]}</td>
    })
}