import React from 'react';
import logo from './logo.svg';
import './App.css';
import CityForm from './CityForm';
import CityTable from './CityTable';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <input type="text" id="city" name="city"></input> */}
        <CityForm></CityForm>
      </header>
    </div>
  );
}

export default App;
