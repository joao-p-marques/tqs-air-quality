
## How to run

```
pipenv install
pipenv shell
```

`geckodriver` needs to be on $PATH

```
ln -s $PWD/geckodriver /home/jota/.local/share/virtualenvs/ui_tests-7xUyLTLR/bin
```

Run the test:
```
pytest
```
