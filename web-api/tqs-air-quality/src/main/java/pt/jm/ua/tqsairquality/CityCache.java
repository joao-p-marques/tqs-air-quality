package pt.jm.ua.tqsairquality;

import java.util.HashMap;

import pt.jm.ua.tqsairquality.city.City;

public class CityCache {

    HashMap<String, City> map;
    int size_limit;

    // @Autowired
    // ExternalAPIBridge api_bridge;

    public CityCache(int size_limit) {
      this.size_limit = size_limit;
      this.map = new HashMap<>();
    }

    public CityCache() {
      this(10);
    }

    public boolean has_city(String city_name) {
        return this.map.containsKey(city_name);
    }

    // public City get_city(String city) {
    //     if (this.map.containsKey(city)) { // fetch from cache
    //         return this.map.get(city);
    //     }
    //     else {
    //         String data = api_bridge.get_current_data(city);
    //         City city_data = this.process_data(data);
    //         this.add_to_cache(city, city_data);
    //         return city_data;
    //     }
    // } 

    public City get_city(String city_name) {
        if (this.map.containsKey(city_name))
            return this.map.get(city_name);
        else
            return null;
    }

    public void add_to_cache(String city_name, City city_data) {
        if (this.isFull()) {
            this.map.remove(this.map.keySet().toArray()[0]);
        }
        this.map.put(city_name, city_data);
    }

    public int size() {
        return this.map.size();
    }

    public boolean isEmpty() {
        return (this.size() == 0);
    }

    public boolean isFull() {
        return (this.size() == this.size_limit);
    }

    // private City process_data(final String data) {
    //     final ObjectMapper mapper = new ObjectMapper();
    //     try {
    //         City city = mapper.readValue(data, City.class);
    //         System.out.println(city.getData().getCity());
    //         return city;
    //     } catch (JsonMappingException e) {
    //         // TODO Auto-generated catch block
    //         e.printStackTrace();
    //     } catch (JsonProcessingException e) {
    //         // TODO Auto-generated catch block
    //         e.printStackTrace();
    //     }
    //     return null;
    // }

}