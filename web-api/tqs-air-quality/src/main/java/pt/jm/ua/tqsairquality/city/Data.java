
package pt.jm.ua.tqsairquality.city;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "aqi",
    "idx",
    "attributions",
    "city",
    "dominentpol",
    "iaqi",
    "time",
    "debug"
})
public class Data {

    @JsonProperty("aqi")
    private Integer aqi;
    @JsonProperty("idx")
    private Integer idx;
    @JsonProperty("attributions")
    private List<Attribution> attributions = null;
    @JsonProperty("city")
    private City_ city;
    @JsonProperty("dominentpol")
    private String dominentpol;
    @JsonProperty("iaqi")
    private Iaqi iaqi;
    @JsonProperty("time")
    private Time time;
    @JsonProperty("debug")
    private Debug debug;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param iaqi
     * @param debug
     * @param city
     * @param aqi
     * @param time
     * @param idx
     * @param attributions
     * @param dominentpol
     */
    public Data(Integer aqi, Integer idx, List<Attribution> attributions, City_ city, String dominentpol, Iaqi iaqi, Time time, Debug debug) {
        super();
        this.aqi = aqi;
        this.idx = idx;
        this.attributions = attributions;
        this.city = city;
        this.dominentpol = dominentpol;
        this.iaqi = iaqi;
        this.time = time;
        this.debug = debug;
    }

    @JsonProperty("aqi")
    public Integer getAqi() {
        return aqi;
    }

    @JsonProperty("aqi")
    public void setAqi(Integer aqi) {
        this.aqi = aqi;
    }

    @JsonProperty("idx")
    public Integer getIdx() {
        return idx;
    }

    @JsonProperty("idx")
    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @JsonProperty("attributions")
    public List<Attribution> getAttributions() {
        return attributions;
    }

    @JsonProperty("attributions")
    public void setAttributions(List<Attribution> attributions) {
        this.attributions = attributions;
    }

    @JsonProperty("city")
    public City_ getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(City_ city) {
        this.city = city;
    }

    @JsonProperty("dominentpol")
    public String getDominentpol() {
        return dominentpol;
    }

    @JsonProperty("dominentpol")
    public void setDominentpol(String dominentpol) {
        this.dominentpol = dominentpol;
    }

    @JsonProperty("iaqi")
    public Iaqi getIaqi() {
        return iaqi;
    }

    @JsonProperty("iaqi")
    public void setIaqi(Iaqi iaqi) {
        this.iaqi = iaqi;
    }

    @JsonProperty("time")
    public Time getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Time time) {
        this.time = time;
    }

    @JsonProperty("debug")
    public Debug getDebug() {
        return debug;
    }

    @JsonProperty("debug")
    public void setDebug(Debug debug) {
        this.debug = debug;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
