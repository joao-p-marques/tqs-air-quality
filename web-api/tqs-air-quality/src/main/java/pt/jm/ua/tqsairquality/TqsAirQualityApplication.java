package pt.jm.ua.tqsairquality;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class TqsAirQualityApplication {

	public static void main(String[] args) {
		SpringApplication.run(TqsAirQualityApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public ExternalAPIBridge externalAPIBridge() {
		return new ExternalAPIBridge();
	}

	@Bean
	public CityCache city_cache() {
		return new CityCache();
	}
}
