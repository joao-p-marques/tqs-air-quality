package pt.jm.ua.tqsairquality;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import pt.jm.ua.tqsairquality.city.*;

@RestController
@CrossOrigin(origins = "*")
public class AirQualityController {

    @Autowired
    ExternalAPIBridge api_bridge;

    @Autowired
    CityCache city_cache;

    @GetMapping("/test")
    public City test() {
        final String received_data = api_bridge.get_current_data("shanghai");
        return process_data_to_city(received_data);
    }

    @GetMapping("/info")
    public City city(@RequestParam(value = "city", defaultValue = "shanghai") String city, 
                    @RequestParam(value = "lat", defaultValue = "") String lat, 
                    @RequestParam(value = "lng", defaultValue = "") String lng
    ){
        City res = null;

        System.out.println(city);

        if (!lat.equals("") && !lng.equals(""))
            return coords(lat, lng);

        res = check_cache(city);
        if (res == null) {
            final String received_data = api_bridge.get_current_data(city);
            res = process_data_to_city(received_data);
            if (res == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "City not found");
            }
            else {
                this.update_cache(city, res);
            }
        }
        else {
            System.out.println("Using cache");
        }

        return res;
    }

    public City coords(String lat, String lng) {
        // no cache for lat/lng
        String received_data = api_bridge.get_current_data(lat, lng);
        return process_data_to_city(received_data);
    }

    public City check_cache(String city_name) {
        return this.city_cache.get_city(city_name);
    }

    public void update_cache(String city_name, City city_data) {
        this.city_cache.add_to_cache(city_name, city_data);
    }

    public City process_data_to_city(final String data) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            City city = mapper.readValue(data, City.class);
            System.out.println(city.getData().getCity());
            return city;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            System.out.println("City not found");
        }
        return null;
    }

}
