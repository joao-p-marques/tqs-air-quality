package pt.jm.ua.tqsairquality;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class ExternalAPIBridge {

    @Autowired
    RestTemplate restTemplate;
    
    String baseURL = "http://api.waqi.info/";

    public String get_current_data(String city) {
        String url = baseURL + "feed/" + city + "/?token=" + System.getenv("AQICN_TOKEN");
        return this.restTemplate.getForObject(url, String.class);
    }

    public String get_current_data(String lat, String lng) {
        String url = baseURL + "feed/geo:" + lat + ";lng:" + lng + "/?token=" + System.getenv("AQICN_TOKEN");
        return this.restTemplate.getForObject(url, String.class);
    }

}