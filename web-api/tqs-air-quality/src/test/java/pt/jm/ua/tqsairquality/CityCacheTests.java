package pt.jm.ua.tqsairquality;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import pt.jm.ua.tqsairquality.city.City;

@SpringBootTest
class CityCacheTests {

    // @Mock
    // private ExternalAPIBridge api_bridge;

    // @InjectMocks
    CityCache cache;

    // @BeforeEach
    // void setup_mock() {
    //     String api_return_value = "";
    //     try {
    //         api_return_value = Files.readString(Paths.get("src/test/java/pt/jm/ua/tqsairquality/resources/test.json"), StandardCharsets.UTF_16);
    //     } catch (IOException e) {
    //         // TODO Auto-generated catch block
    //         e.printStackTrace();
    //     }

    //     when(api_bridge.get_current_data(anyString())).thenReturn(api_return_value);
    // }

    @BeforeEach
    void setup_cache() {
        this.cache = new CityCache();
    }

    @Test
    @DisplayName("Chache should start empty")
    void test_cache_empty() {
        Assertions.assertTrue(this.cache.isEmpty());
    }

    @Test
    @DisplayName("Chache should not be empty after 1st call")
    void test_cache_not_empty() {
        this.cache.add_to_cache("Shanghai", new City());
        Assertions.assertFalse(this.cache.isEmpty());
    }

    @Test
    @DisplayName("Test cache size limit")
    void test_size_limit() {
        this.cache = new CityCache(2);
        this.cache.add_to_cache("Shanghai", new City()); 
        this.cache.add_to_cache("London", new City()); 
        Assertions.assertTrue(this.cache.isFull());
    }

    @Test
    @DisplayName("Calling the same city twice should have city in cache")
    void test_cache_caches () {
        this.cache.add_to_cache("Shanghai", new City());
        Assertions.assertTrue(this.cache.has_city("Shanghai"));
    }

    @Test
    @DisplayName("Calling new city should not have in cache")
    void test_cache_does_not_cache() {
        Assertions.assertFalse(this.cache.has_city("Shanghai"));
    }

    @Test
    @DisplayName("Adding city should increase size")
    void test_size_increases() {
        // initial size is zero
        this.cache.add_to_cache("Shanghai", new City());
        Assertions.assertNotEquals(0, this.cache.size());
    }

    @Test
    @DisplayName("Size stays the same when reaching size limit")
    void size_stays_the_same() {
        this.cache = new CityCache(2);
        this.cache.add_to_cache("Shanghai", new City()); 
        this.cache.add_to_cache("London", new City()); 
        int size = this.cache.size(); // should be 2
        this.cache.add_to_cache("Paris", new City()); 
        Assertions.assertEquals(size, this.cache.size());
    }
}

