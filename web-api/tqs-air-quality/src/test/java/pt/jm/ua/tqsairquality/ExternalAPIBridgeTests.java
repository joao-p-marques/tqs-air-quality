package pt.jm.ua.tqsairquality;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
class ExternalAPIBridgeTests {

    @Mock
    private RestTemplate restTemplate;

    @Autowired
    @InjectMocks
    ExternalAPIBridge bridge;

    @BeforeEach
    void setup_mock() {
        String baseURL_working = "http://api.waqi.info/feed/shanghai/?token=" + System.getenv("AQICN_TOKEN");
        String baseURL_not_working = "http://api.waqi.info/feed/aveiro/?token=" + System.getenv("AQICN_TOKEN");

        when(restTemplate.getForObject(baseURL_working, String.class)).thenReturn(new ResponseEntity<>(HttpStatus.OK).toString());
        when(restTemplate.getForObject(baseURL_not_working, String.class)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND).toString());
    }

    @Test
    @DisplayName("Test Mock")
    public void test_mock() {
        String res = restTemplate.getForObject("", String.class);
        System.out.println(res);
    }

    @Test
    @DisplayName("Bridge should return String")
    public void test_returns_string() {
        Object res = bridge.get_current_data("shanghai");
        assertTrue(res instanceof String);
    }

    @Test
    @DisplayName("Bridge should return JSON data when valid call")
    public void test_returns_jsonable() {
        String res = bridge.get_current_data("shanghai");
        assertTrue(isJSONValid(res));
    }

    // @Test
    // @DisplayName("Bridge should return error when invalid call")
    // public void test_returns_error() {
    //     String res = bridge.get_current_data("aveiro");
    //     assertFalse(isJSONValid(res));
    // }

    private boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    @Test
    @DisplayName("Bridge should return status OK when city exists")
    public void test_returns_ok() {
        String res = bridge.get_current_data("shanghai");
        System.out.println(res);
        JSONObject json;
        String status;
        try {
            json = new JSONObject(res);
            System.out.println(json);
            status = json.getString("status");
        } catch (JSONException e) {
            status = "error";
        }
        assertEquals("ok", status);
    }

    @Test
    @DisplayName("Bridge should return status ERROR when city doesn't exist")
    public void test_returns_error() {
        String res = bridge.get_current_data("aveiro");
        JSONObject json;
        String status;
        try {
            json = new JSONObject(res);
            status = json.getString("status");
        } catch (JSONException e) {
            status = "error";
        }
        assertEquals("error", status);
    }
}
