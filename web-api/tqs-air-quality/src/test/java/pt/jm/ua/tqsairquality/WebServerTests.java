package pt.jm.ua.tqsairquality;

import javax.net.ssl.SSLEngineResult.Status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest
@AutoConfigureMockMvc
public class WebServerTests {

	@Autowired
	private MockMvc mockMvc;

    @Test
    @DisplayName("Test test endpoint")
	public void test_should_return_data() throws Exception {
        this.mockMvc.perform(get("/test")).andExpect(status().isOk())
                                          .andExpect(content().string(containsString("city"))); 
    }
    
    @Test
    @DisplayName("Ensure info endpoint returns city")
    public void info_returns_city() throws Exception {
        this.mockMvc.perform(get("/info?city=shanghai"))
                        .andExpect(status().isOk())
                        .andExpect(content().string(containsString("city")));
    }
    
    @Test
    @DisplayName("Ensure info endpoint returns 404 when city doesn't exist")
    public void info_returns_not_found() throws Exception {
        this.mockMvc.perform(get("/info?city=aveiro"))
                        .andExpect(status().isNotFound());
    }
}