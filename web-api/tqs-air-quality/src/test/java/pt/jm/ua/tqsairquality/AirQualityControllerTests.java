package pt.jm.ua.tqsairquality;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import pt.jm.ua.tqsairquality.city.City;

@SpringBootTest
public class AirQualityControllerTests {

    @Autowired
    @InjectMocks
    AirQualityController controller;

    @Mock
    ExternalAPIBridge api_bridge;

    @Mock 
    CityCache city_cache;

    String api_return_data = "{\"status\":\"ok\",\"data\":{\"aqi\":85,\"idx\":1437,\"attributions\":[{\"url\":\"https://sthj.sh.gov.cn/\",\"name\":\"Shanghai Environment Monitoring Center\"},{\"url\":\"http://106.37.208.233:20035/emcpublish/\",\"name\":\"China National Urban air quality real-time publishing platform\"},{\"url\":\"https://china.usembassy-china.org.cn/embassy-consulates/shanghai/air-quality-monitor-stateair/\",\"name\":\"U.S. Consulate Shanghai Air Quality Monitor\"},{\"url\":\"https://waqi.info/\",\"name\":\"World Air Quality Index Project\"}],\"city\":{\"geo\":[31.2047372,121.4489017],\"name\":\"Shanghai\",\"url\":\"https://aqicn.org/city/shanghai\"},\"dominentpol\":\"pm25\",\"iaqi\":{\"co\":{\"v\":4.7},\"h\":{\"v\":76},\"no2\":{\"v\":13.3},\"o3\":{\"v\":46.8},\"p\":{\"v\":1016.8},\"pm10\":{\"v\":47},\"pm25\":{\"v\":85},\"so2\":{\"v\":2.6},\"t\":{\"v\":15.7},\"w\":{\"v\":0.2}},\"time\":{\"s\":\"2020-04-10 18:00:00\",\"tz\":\"+08:00\",\"v\":1586541600},\"debug\":{\"sync\":\"2020-04-10T20:26:16+09:00\"}}}";

    @BeforeEach
    void setup_mocks() {
        // city that exists
        when(api_bridge.get_current_data("shanghai")).thenReturn(api_return_data);
        // city that doens't exist
        when(api_bridge.get_current_data("aveiro")).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND).toString());

        when(city_cache.get_city("cached_city")).thenReturn(new City());
        when(city_cache.get_city("not_cached_city")).thenReturn(null);

        doNothing().when(city_cache).add_to_cache(anyString(), any(City.class));
    }

    @Test
    @DisplayName("Ensure controller started")
	public void contex_loads() throws Exception {
        assertNotNull(controller);
    }
    
    @Test
    @DisplayName("Test JSON to City Mapper")
    public void test_json_mapper() {
        City res = controller.process_data_to_city(api_return_data);
        assertNotNull(res);
    }

}